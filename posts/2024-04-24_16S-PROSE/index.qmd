---
title: "Metabarcoding analyses:<br>from sequences to plots."
subtitle: "Formation PROSE"
author:
  - name: "Cédric Midoux"
    orcid: "0000-0002-7964-0929"
    affiliations: "PROSE & MaIAGE"
date: "2024-04-24"
date-format: long
format:
  inrae-revealjs:
    toc: false
    self-contained: false
    # header-logo: "https://easy16s.migale.inrae.fr/logo.png"
    # header-logo-link: "https://easy16s.migale.inrae.fr/"
    footer: "Formation PROSE -- 24 avril 2024"
title-slide-attributes: 
  data-background-image: ../../_extensions/davidcarayon/inrae/ressources/assets/sigle-inrae.png
  data-background-size: 40%
  data-background-position: left
  data-background-opacity: "0.5"
  data-footer: "<a rel='license' href='http://creativecommons.org/licenses/by-sa/2.0/'><img alt='Creative Commons License' style='border-width:0' src='https://i.creativecommons.org/l/by-sa/2.0/88x31.png' /><br></a>This work is licensed under a <a rel='license' href='http://creativecommons.org/licenses/by-sa/2.0/'>Creative Commons Attribution-ShareAlike 2.0 Generic License</a>."
# filters:
#   - reveal-header
categories: 
  - Metabarcoding
  - Easy16S
image: "https://frogs.toulouse.inra.fr/img/frogs.png"
draft: false
license: "CC BY-SA"
editor: visual    
bibliography: references.bib
---

# Introduction to amplicon analyses

## Road to Metagenomics

![@escobar2015road](images/Escobar-Zepeda.jpg){fig-align="center"}

## Meta-omics using next-generation sequencing (NGS)

![](images/frogs001.png){fig-align="center"}

## Meta-omics using next-genertation sequencing (NGS)

![](images/metabarcoding.png){fig-align="center"}

## Strengths

::: incremental
-   Detect subdominant microorganisms present in complex samples → microbial inventories
-   Get (approximate) relative abondances of different taxa in samples
-   Analyze and compare many taxa (hundreds) at the same time
-   Taxonomic profiles of the communities (usually up to genus level, and sometimes up to species or strain)
-   Low cost
:::

## Weaknesses

::: incremental
-   Compositional data, many biases → no absolute quantification
-   Exact identification of the organisms difficult
-   Hard to distinguish live and dead fractions of the communities
-   No functional view of the ecosystem
:::

## Choice of a marker gene

The perfect / ideal gene marker:

-   is ubiquist
-   is conserved among taxa
-   is enough divergent to distinguish stains
-   is not submitted to lateral transfer
-   has only one copy in genome
-   has conserved regions to design specific primers
-   is enough characterized to be present in databases for taxonomic affiliation

## The gene encoding the small subunit of the ribosomal RNA {.smaller}

-   The most widely used gene in molecular phylogenetic studies
-   Ubiquist gene: 16S rDNA in prokayotes ; 18S rDNA in eukaryotes
-   Gene encoding a ribosomal RNA : non-coding RNA (not translated), part of the small subunit of the ribosome which is responsible for the translation of mRNA in proteins
-   Not submitted to lateral gene transfer
-   Availability of databases facilitating comparison
    -   Silva v138.1 - 2021: available SSU/LSU sequences to over 10,700,000

## The 16S resolution {.smaller}

::: columns
::: {.column width="50%"}
![](images/16Sregion1.jpg){height="420"}
:::

::: {.column width="50%"}
![](images/16Sregion2.jpg){height="420"}
:::
:::

::: aside
*Analysis of the taxa recovery rate indicates a great underestimation of taxa richness when partial sequences are used. Although the situation tends to ameliorate as longer segments are considered, near full-length 16S rRNA genes sequences are required for accurate richness estimations and accurate classifications of high taxa.*

@yarza2014uniting: Uniting the classification of cultured and uncultured bacteria and archaea using 16S rRNA gene sequences
:::

## 16S rRNA copy number {.smaller}

::: columns
::: {.column width="50%"}
*Median of the number of 16S rRNA copies in 3,070 bacterial species according to data reported in [rrnDB database](https://rrndb.umms.med.umich.edu) – 2018*
:::

::: {.column width="50%"}
![](images/rRNACNV.jpg)
:::
:::

::: aside
@espejo2018multiple: Multiple ribosomal RNA operons in bacteria; their concerted evolution and potential consequences on the rate of evolution of their 16S rRNA
:::

## 16S rRNA copy variation {.smaller}

![](images/rRNAvariations.png){width="70%" fig-align="center"}

::: columns
::: {.column width="50%"}
\[B\] The positions of sequence variation within 16S and 23S rRNA are shown along the gene organization of rrn operons. A total of 33 and 77 differences were identified in 16S rRNA and 23S rRNA, respectively.
:::

::: {.column width="50%"}
\[C\] The number of bases that are different from the conserved sequence are shown for 16S and 23S rRNA for each rrn operon
:::
:::

::: aside
Maeda et al., 2015: Strength and regulation of seven rRNA promoters in Escherichia coli @maeda2015strength
:::

## 16S rRNA copy variation {.smaller}

-   Only a minority of bacterial genomes harbors identical 16S rRNA gene copies
-   Sequence diversity increases with increasing copy numbers
-   While certain taxa harbor dissimilar 16S rRNA genes, others contain sequences common to multiple species

# Planning an experiment

## Challenges

![](images/challenges.png){height="480px" fig-align="center"}

::: aside
@10.1093/bib/bbz155: Current challenges and best-practice protocols for microbiome analysis
:::

## Sequencing technologies

![](images/sequencing_technos.jpg)

## Sequencing technologies

![](images/sequencing_comparison_2022.png){fig-align="center"}

::: aside
@tan2022bioinformatics: Bioinformatics approaches and applications in plant biotechnology
:::

## Illumina technology

{{< video https://youtu.be/fCd6B5HRaZ8 width="100%" height="85%" >}}

## Illumina technology

![](images/illumina.png){height="500" fig-align="center"}

## Illumina technology

![](images/illumina_two_pcr.jpg){fig-align="center" height="480"}

::: aside
@cruaud2017high: High-throughput sequencing of multiple amplicons for barcoding and integrative taxonomy
:::

## Sequencing biases {.smaller}

::: columns
::: column
-   Contamination between samples during the same run
-   Contamination during successive runs (residual contaminants)
-   Variability between runs: take into account for experimental plan
-   Variability inside run: add some controls

::: aside
@Salter2014: Reagent and laboratory contamination can critically impact sequence-based microbiome analyses
:::
:::

::: column
![](images/frogs018.png){height="420" fig-align="center"}

::: aside
@whon2018effects: The effects of sequencing platforms on phylogenetic resolution in 16 S rRNA gene profiling of human feces
:::
:::
:::

## Synthetis of biases

![](images/frogs036.png){height="480" fig-align="center"}

::: aside
@brooks2015truth: The truth about metagenomics: quantifying and counteracting bias in 16S rRNA studies
:::

## Synthesis

![](images/view_biases.png)

# Bioinformatics

## A pile of pipelines {.smaller}

::: columns
::: {.column width="36%"}
![](images/a_pile_of_pipelines_3.jpg){height="500"}
:::

::: {.column width="64%"}
![](images/a_pile_of_pipelines_2.png)
:::
:::

::: aside
@hakimzadeh2023pile: A pile of pipelines: An overview of the bioinformatics software for metabarcoding data analyses
:::

# Conclusion 1: *sequencing data do not contain exactly what you sampled ...*

## Summary

![](images/frogs012.png)

# Conclusion 2: *... but you now know how to deal with*

# FROGS <br> *Find, Rapidly, OTUs with Galaxy Solution*

## FROGS team

-   FROGS is a INRAE development project

![](images/FROGS_logo.png){.absolute top="0" right="100" width="75"}

![](images/frogs-team.png)

## FROGS articles

::: columns
::: column
![@frogs](images/frogs2018.png){height="500"}
:::

::: column
![@frogsITS](images/frogs2021.png){height="500"}
:::
:::

## How to use FROGS

![](images/FROGS_logo.png){.absolute top="0" right="100" width="75"}

::: columns
::: {.column width="50%"}
-   Command line {{< fa code >}}

```         
remove_chimera.py 
  --input-biom clustering.biom \ 
  --input-fasta clustering.fasta \
  --non-chimera remove_chimera.fasta \
  --out-abundance remove_chimera.biom \
  --summary remove_chimera.html
```
:::

::: {.column width="50%"}
-   Galaxy instances via web {{< fa globe >}} ![](images/frogs-galaxy-ex.png){fig-align="center"}
:::
:::

::: aside
-   Migale - <https://galaxy.migale.inrae.fr/>
-   Genotoul - <https://vm-galaxy-prod.toulouse.inrae.fr/>
-   IFB - <https://metabarcoding.usegalaxy.fr/>
:::

## FROGS docs and help

![](images/FROGS_logo.png){.absolute top="0" right="100" width="75"}

-   {{< fa globe >}} Website: <https://frogs.toulouse.inrae.fr>
-   {{< fa brands github >}} Github: <https://github.com/geraldinepascal/FROGS.git>
-   {{< fa newspaper >}} Newsletter: *subscription request at* [frogs-support\@inrae.fr](mailto:frogs-support@inrae.fr)
-   {{< fa question >}} Need help
    -   [frogs-support\@inrae.fr](mailto:frogs-support@inrae.fr) for generic questions
    -   [help-migale\@inrae.fr](mailto:help-migale@inrae.fr) for bugs/quotas/errors with Galaxy Migale instance

## [TP1: Introduction to Galaxy](https://documents.migale.inrae.fr/posts/training-materials/2023-09-11-module20/TP/#introduction-%C3%A0-galaxy)

![](images/patrick-star-idk.gif){height="560" fig-align="center"}

# Sequencing data

## FASTQ format

```         
@ST-E00114:1342:HHMGVCCX2:1:1101:3123:2012 1:N:0:TCCGGAGA+TCAGAGCC
CTTGGTCATTTAGAG
+
***<<*AEF???***
@ST-E00114:1342:HHMGVCCX2:1:1101:11556:2030 1:N:0:TCCGGAGA+TCAGAGCC
CATTGGCCATATCAT
+
AAAE??<<*???***
```

Meaning

```         
@Identifier1 (comment)
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
+
QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
@Identifier2 (comment)
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
+
QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
```

## Quality score encoding

![](images/quality_score_encoding.png)

## Quality score

Measure of the quality of the identification of the nucleobases generated by automated DNA sequencing

![](images/phred_meaning.png)

## FASTQ compression

-   Compression is essential to deal with FASTQ files (reduce disk storage)
-   *extension*: `file.fastq.gz`
-   Tools are (almost all) able to deal with compressed files {{< fa thumbs-up >}}

# Quality control

## Quality control

-   One of the most easy step in bioinformatics ...
-   ... but one of the most important
-   check if everything is ok
-   Indicates if/how to clean reads
-   Shows possible sequencing problems
-   The results must be interpreted in relation to what has been sequenced

## Reads are not perfect

![](images/reads_not_perfect.png)

::: aside
Sequencing error profiles of Illumina sequencing instruments @10.1093/nargab/lqab019
:::

## Why QC'ing your reads?

Try to answer to (not always) simple questions:

-   Are data conform to the expected level of performance?
    -   Size / Number of reads / Quality
-   Residual presence of adapters or indexes?
-   (Un)expected techincal biases?
-   (Un)expected biological biases?

::: callout-warning
QC without context leads to misinterpretation!
:::

## [TP2: Quality control](https://documents.migale.inrae.fr/posts/training-materials/2023-09-11-module20/TP/#contr%C3%B4le-qualit%C3%A9-des-donn%C3%A9es-de-s%C3%A9quen%C3%A7age)

![](images/patrick-star-idk.gif){height="560" fig-align="center"}

# Preprocess

## What FROGS preprocess does? {.smaller}

-   Merging of R1 and R2 reads with `vsearch` @vsearch, `flash` @flash or `pear` @pear (only in command line)
-   Deletes sequences without good primers
-   Finds and removes adapter sequences with cutadapt
-   Deletes sequence with not expected lengths
-   Deletes sequences with ambiguous bases (N)
-   Dereplication
-   *removing homopolymers (size = 8) for 454 data*
-   *quality filter for 454 data*

## Merging of paired-end reads

![](images/frogs_overlap.png){fig-align="center"}

## [TP FROGS preprocess](https://documents.migale.inrae.fr/posts/training-materials/2023-09-11-module20/TP/#frogs-preprocess-iconify-mdi-tools)

![](images/patrick-star-idk.gif){height="560" fig-align="center"}

# Clustering

## Sequencing data are noised

![](images/frogs024.png)

## How to deal with these noised sequences?

-   Comparison all against all
    -   Very accurate
    -   Requires a lot of memory and/or time
-   Clustering
    -   closed-reference / open-reference
    -   de novo
-   Denoising

## Vocabulary {.smaller}

-   A lot of terms for features built by softwares
    -   OTUs, zOTUS, ASVs, ESVs...
-   A recent review establishes the vocabulary @hakimzadeh2023pile
    -   OTUs / ASVs / swarm clusters

> ASVs are identical denoised reads with as few as 1 base pair difference between variants, representing an inference of the biological sequences prior to amplification and sequencing errors.

-   OTUs are formed with a % threshold clustering
-   Swarm clusters are a third feature type

## OTU paradigm

-   Operational Taxonomic Unit

![](images/frogs_otu_paradigm.png)

## Operational Taxonomic Units

![](images/frogs025.png)

## Operational Taxonomic Units

![](images/frogs013.png){fig-align="center" height="500"}

## Operational Taxonomic Units

![](images/frogs026.png)

## ASV paradigm {.smaller}

-   Amplicon Sequence Variants

![](images/frogs_asv_paradigm.png)

ASV are inferred by a de novo process in which biological sequences are discriminated from errors on the basis of the expectation that biological sequences are more likely to be repeatedly observed than are error-containing sequences

## ASV resolution

![](images/frogs_asv_resolution.png)

-   ASV resolution changes the composition for these samples

::: aside
Credits: https://benjjneb.github.io/dada2/SMBS_DADA2.pdf
:::

## Swarm: A smart idea

![](images/frogs028.png){fig-align="center" height="500"}

::: aside
@swarm: Swarm v2: highly-scalable and high-resolution amplicon clustering
:::

## Why Swarm?

-   Fixed clustering threshold is a real problem
-   OTUs construction is input-order depenent

![](images/frogs027.png){fig-align="center" height="400"}

## `d`: the small local linking threshold

![](images/frogs029.png)

## Swarm steps

![](images/frogs030.png)

## Swarm {.smaller}

-   A robust and fast clustering method for amplicon-based studies
-   The purpose of swarm is to provide a novel clustering algorithm to handle large sets of amplicons
-   swarm results are resilient to input-order changes and rely on a small local linking threshold d, the maximum number of differences between two amplicons
-   swarm forms stable high-resolution clusters, with a high yield of biological information
-   Default: forms a lot of low-abundant OTUs that are in fact artifacts and need to be removed
-   **Swarm (fastidious method + *d=1*) clusters + filters → ASVs**

## [TP FROGS clustering](https://documents.migale.inrae.fr/posts/training-materials/2023-09-11-module20/TP/#frogs-clustering-iconify-mdi-tools)

![](images/patrick-star-idk.gif){height="560" fig-align="center"}

# Chimera removal

## Chimera removal

::: columns
::: {.column width="50%"}
![](images/frogs_chimera.jpg)
:::

::: {.column width="50%"}
![](images/chimera.gif)
:::
:::

## Chimera detection strategies

-   Reference based: against a database of «genuine» sequences

    -   dependant of the references used

-   De novo: against abundant sequences in the samples {{< fa thumbs-up >}}

-   FROGS uses `vsearch` @vsearch as chimera removal tool

## Sample-cross validation

-   FROGS adds a sample-cross validation

![](images/frogs031.png)

## [TP FROGS remove chimera](https://documents.migale.inrae.fr/posts/training-materials/2023-09-11-module20/TP/#frogs-remove-chimera-iconify-mdi-tools)

![](images/patrick-star-idk.gif){height="560" fig-align="center"}

# Abundance/Prevalence filters

## How to filter clusters?

-   Low abundant sequences
-   Clusters not shown in few replicates
-   Contamination

## [TP FROGS cluster filters](https://documents.migale.inrae.fr/posts/training-materials/2023-09-11-module20/TP/#frogs-cluster-filters-iconify-mdi-tools)

![](images/patrick-star-idk.gif){height="560" fig-align="center"}

# Taxonomic affiliation

## The FROGS databanks

-   Affiliation with `blast`
    -   Check Blast metrics to avoid concluding too fast
    -   Take care of the reference databank used!
-   Command line: you can use your own databank
-   Galaxy
    -   You have access to [several databanks](http://genoweb.toulouse.inra.fr/frogs_databanks/assignation/readme.txt)
    -   Admins have to add your databank

## The multi-affiliations

-   FROGS gives all identical hits

```         
Bacteria;Firmicutes;Bacilli;Staphylococcales;Staphylococcaceae;Staphylococcus;Staphylococcus xylosus
Bacteria;Firmicutes;Bacilli;Staphylococcales;Staphylococcaceae;Staphylococcus;Staphylococcus saprophyticus
```

*Strictly identical (V1-V3 amplification) on 499 nucleotides*

-   FROGS can't decide if it's one or another
-   You have to check if you can choose between multi-affiliations

```         
Bacteria;Firmicutes;Bacilli;Staphylococcales;Staphylococcaceae;Staphylococcus;Multi-affiliations
```

## To help you

-   <https://shiny.migale.inrae.fr/app/affiliationexplorer>
-   a very user-friendly Shiny web app, allowing users to modify very simply the affiliations from a FROGS abundance file

## [TP FROGS Taxonomic affiliation](https://documents.migale.inrae.fr/posts/training-materials/2023-09-11-module20/TP/#frogs-taxonomic-affiliation-iconify-mdi-tools)

![](images/patrick-star-idk.gif){height="560" fig-align="center"}

## Affiliation filters

-   Remaining contamination?
-   Want to analyse only the `Firmicutes`?
-   2 modes
    -   *Deleting*: remove ASVs
    -   *Hiding*: only the affiliation is modified, not the abundance

# Phylogenetic tree

## FROGS tree {.smaller}

::: columns
::: {.column width="50%"}
-   This tool builds a phylogenetic tree thanks to affiliations of ASVs contained in the BIOM file
-   Needed to compute beta-diversity indices based on phylogenetic distances
-   Interesting to explore poor-characterized environments
:::

::: {.column width="50%"}
![](images/frogs039.png){height="500" fig-align="center"}
:::
:::

## [TP FROGS Tree](https://documents.migale.inrae.fr/posts/training-materials/2023-09-11-module20/TP/#frogs-tree)

![](images/patrick-star-idk.gif){height="560" fig-align="center"}

# Conclusion of bioinformatics analysis

## FROGS Procedure

![](images/sop16S.png){height="500" fig-align="center"}

## FROGS Output

-   BIOM file
    -   Count matrix for each ASV for each sample
    -   Taxonomic affiliation table for each ASV
-   Multiaffiliation table
-   FASTA file (Representative sequence for each ASV)
-   Newick file (Phylogenetic tree)
-   Many HTML report

<!-- -->

-   Metadata table for each sample to be added

## Road to statistics

![<https://joey711.github.io/phyloseq/>](images/phyloseq-data.png){height="500" fig-align="center"}

# Analysis of community composition

## Easy16S

::: columns
::: column
-   a user-friendly Shiny web-service for exploration and visualization of microbiome data
-   Documentation: <https://easy16s.migale.inrae.fr>
-   Server: <https://shiny.migale.inrae.fr/app/easy16S>
:::

::: column
![](https://easy16s.migale.inrae.fr/logo.png){fig-align="center"}
:::
:::

## Data import

-   Demo dataset
-   Build from FROGS
    -   BIOM
    -   Metadata
    -   Phylogenetic tree
-   RData/RDS

## Preprocess data

-   Samples names
-   Sample variables (metadata)
-   Depth (samples sums)
-   Aggregate taxa
-   Spread taxonomy
-   Rarefaction
-   Transform the abundances using `fun`

# Data structure

## Summary

![](images/easy16s_summary.png){height="550" fig-align="center"}

## Table

-   phyloseq data structure
    -   OTU Table
    -   Taxomony Table
    -   Agglomerate OTU Table
    -   Sample Data Table
        -   metadata with `esquisse`
    -   a phylogenetic tree (*not viewable*)
    -   a set of reference sequences (*not viewable*)

## Barplot

![](images/easy16s_barplot.png){height="550" fig-align="center"}

## Rarefaction curves

![](images/easy16s_rarefaction.png){height="550" fig-align="center"}

## Heatmap composition

![](images/easy16s_heatmap.png){height="550" fig-align="center"}

# Biodiversity indices

## Biodiversity indices

-   Different kinds of biodiversity indices...
    -   α-diversity: diversity within a community
    -   β-diversity: diversity between communities
    -   γ-diversity: diversity at the landscape scale (blurry for bacterial communities)

## Based on different types of data

-   Presence/Absence (qualitative) vs. Abundance (quantitative)
    -   Presence/Absence gives less weight to **dominant species**
    -   is more **sensitive** to differences in sampling depths
    -   emphasizes difference in taxa diversity rather than differences in composition
-   Compositional vs. Phylogenetic
    -   Compositional does not require a **phylogenetic tree**
    -   is more sensitive to erroneous otu picking
    -   gives the same importance to all otus

## α-diversities - *indices* {.smaller}

Note $c_i$ the number of species observed $i$ times ($i = 1, 2, \ldots$) and $p_s$ the proportion of species $s$ ($s = 1, \ldots, S$)

::: fragment
-   **Species richness**: number of observed otus
    -   $S_{rich} = \sum_s1_{p_s>0}=\sum_{i}c_i$
:::

::: fragment
-   **Chao1**: number of observed otu + estimate of the number of unobserved otus
    -   $S_{Chao} = S_{rich}+\hat{c}_0$
:::

::: fragment
-   **Shannon entropy**: the width of the otu relative abundance distribution. Roughly, it reflects our (in)ability to predict the otu of a randomly picked bacteria
    -   $S_{Shan}=-\sum_sp_s\log(p_s)\leq\log(S)$
:::

::: fragment
-   **Inverse Simpson**: inverse of the probability that two bacteria picked at random belong to the same otu.
    -   $S_{InvSimp}=\frac{1}{p^2_1 + \ldots + p^2_S}\leq S$
:::

## α-diversities

![](images/alpha_obs.png){height="250" fig-align="center"}

::: fragment
![](images/alpha_shannon.png){height="250" fig-align="center"}
:::

## β-dissimilarities - *compositional*

Note $\color{red}{n_s^1}$ the count of species $s$ ($s = 1, \ldots , S$ ) in $\color{red}{community \space 1}$ and $\color{blue}{n_s^2}$ the count in $\color{blue}{community \space 2}$.

-   **Jaccard**: Fraction of species specific to either 1 or 2
-   **Bray-Curtis**: Fraction of the community specific to 1 or to 2

![](images/beta_compositional.png){height="300" fig-align="center"}

## β-dissimilarities - *phylogenetic*

For each branch $e$, note $l_e$ its length and $\color{red}{p_e}$ (resp. $\color{blue}{q_e}$) the fraction of $\color{red}{community \space 1}$ (resp. $\color{blue}{community \space 2}$) below branch $e$.

-   **Unifrac**: Fraction of tree specific to either 1 or 2
-   **Weighted Unifrac**: Fraction of the diversity specific to 1 or to 2

![](images/beta_phylogenetic.png){height="300" fig-align="center"}

## β-dissimilarities - *Compo vs. Quali*

::: callout-tip
Different distances capture different features of the samples.\
There is no "one size fits all"!
:::

# Community structure

## Ordination

### Principal Component Analysis (PCA)

-   Each community is described by **otus abundances**
-   Otus abundance maybe **correlated**
-   PCA finds linear combinations of otus that
    -   are uncorrelated
    -   capture well the variance of community composition

> But variance is not a very good measure of β-diversity.

## MultiDimensional Scaling (MDS)

-   Start from a distance matrix $D = (d_{ij})$
-   Project the communities $Com_i \mapsto X_i$ in a euclidian space such that distances are preserved $\lVert X_i - X_j \rVert \approx d_{ij}$

## MultiDimensional Scaling (MDS)

![](images/easy16s_mds.png){height="550" fig-align="center"}

## Hierarchical Clustering

-   Merge closest communities (according to some distance)
-   Update distances between sets of communities using linkage function
-   Repeat until all communities have been merged

### Linkage function {.smaller}

-   `complete`
-   `ward.D2`
-   `single`
-   ...

## Clustering

![](images/easy16s_clustering.png){height="550" fig-align="center"}

# Diversity Partitioning

## Objectives

-   Test composition differences of communities from different groups using a distance matrix
-   Compare within group to between group distances

![](images/multivariate_analysis.png){height="350" fig-align="center"}

## Permutational Multivariate ANOVA

-   Test differences in the community composition of communities from different groups using a distance matrix
-   Permutation is simply the act of rearranging objects
-   ANOVA-like test statistics from a matrix of resemblances

> Does weaning affect community composition?\
> Are groups A and B different?

## Permutational Multivariate ANOVA {.smaller}

### Assumptions

-   Objects in the data set are exchangeable under the null hypothesis
-   Exchangable objects (sites, samples, observations, etc) are independent
-   Exchangable objects have similar multivariate disperson (i.e. each group has a similar degree of multivariate scatter)

### Limitations

-   If groups have different dispersions, *p-value* are not adequate
-   *p-values* computed using permutations, permutations must respect the design

## Differential abundance

-   Comparisons at the global level: is there structure in the data?

> We know that groups A and B are different.\
> How do they differ (in terms of taxa)?

## Differential abundance

### Specificity

-   Negative binomial models were developed for transcriptomics data
-   Normalization assumes that most transcripts are not DA
-   Reasonable for comparison before/after antibiotic intervention
-   Not so when comparing *Soil* against *Seawater*
-   Not clear how negative binomial models cope with this sparsity

## Differential abundance

### Settings

-   Select a *experimental design* and a *contrast*.

### Axis

-   large magnitude fold-changes (log2, x axis)
-   high statistical significance (-log10 of p-value, y axis)

## Volcano plot

![](images/easy16s_volcano.png){height="550" fig-align="center"}

# Conclusion

## Any questions?

![](images/bob_rainbow.gif){height="560" fig-align="center"}

## Bibliography
