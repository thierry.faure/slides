---
title: "Développer ses applications et partager ses résultats avec R et GitLab"
description: "Café des sciences de l'unité BioSP, Avignon"
author: "Cédric Midoux"
date: "2022-02-07"
categories: 
  - Git
  - GitLab
  - R
  - R-packages
  - Shiny
  - Golem
image: "https://forgemia.inra.fr/cedric.midoux/slides/-/raw/without_quarto/2022-02-07_BioSP.png"
---

[Liens vers les slides](https://forgemia.inra.fr/cedric.midoux/slides/-/raw/without_quarto/2022-02-07_BioSP.pdf) de la présentation au café des sciences de l'unité BioSP.
